"""
Prereqs:
    pip install bibtexparser
"""

import bibtexparser
from bibtexparser import bibdatabase
import argparse


def sanitize_title(title):
    return ''.join([x for x in title if x.isalpha() or x.isdigit()]).lower()


def write_output(fp, bibentry):
    outdb = bibdatabase.BibDatabase()
    outdb.entries.append(bibentry)
    bibtexparser.dump(outdb, fp)
    return


def merge_bibfiles(bib_files, output_bib):
    # stores the mapping from primary to seconday key
    ptoskeys = dict()

    distinctkeys = set()
    distincttitles = set()
    titletokeymap = dict()

    outft = open(output_bib, "w")

    for bb in bib_files:
        with open(bb) as fp:
            parser = bibtexparser.bparser.BibTexParser(common_strings=True)
            tmpdb = parser.parse_file(fp)
            tmplist = tmpdb.get_entry_list()

            for bibentry in tmplist:
                bibkey = bibentry['ID']
                bibtitle = sanitize_title(bibentry['title'])

                # if not a distinct key, skip
                if bibkey in distinctkeys:
                    continue

                # if not a distinct title
                if bibtitle in distincttitles:
                    ptoskeys[titletokeymap[bibtitle]].add(bibkey)
                    continue

                # at this point title and key are distinct
                ptoskeys[bibkey] = set()
                titletokeymap[bibtitle] = bibkey
                distinctkeys.add(bibkey)
                distincttitles.add(bibtitle)

                write_output(outft, bibentry)

    outft.close()
    print ("Merged bib file written to %s" % (output_bib))
    return dict([(k,v) for k, v in ptoskeys.items() if len(v) > 0])


def create_sed_replace_script(p2s, opfile):
    with open(opfile, "w") as fp:
        for pkey, skeys in p2s.iteritems():
            for skey in skeys:
                # replace skey with pkey
                fp.write("/cite(.*?)%s/ s/cite\\{(.*?)%s(.*?)\\}/cite\\{\\1%s\\2\\}/g\n" % (skey, skey, pkey))
            pass
    fp.close()
    return


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("bib_files", nargs='+', help="List of bib files to merge")
    parser.add_argument("--exclude", "-x", nargs='*', help="Bib files to exclude")
    parser.add_argument("--output", "-o", default="output.bib", type=str, help="Output bib file")
    args = parser.parse_args()

    if args.exclude:
        bib_files = set(args.bib_files) - set(args.exclude)
    else:
        bib_files = set(args.bib_files)

    print (bib_files)
    p2s = merge_bibfiles(bib_files, args.output)
    create_sed_replace_script(p2s, "%s.sed" % args.output)
